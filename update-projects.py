#!/usr/bin/python
import glob, os, errno, shutil, argparse

################################################################################################# UTILS

def silentremove(filename):
    try:
        os.remove(filename)
    except OSError as e:
        if e.errno == 21: # 21 = is a directory
            shutil.rmtree(filename)
        elif e.errno == 5: # 5 = is a directory
            print("COULD NOT DELETE " + filename)
            print("this seems to be a Windows/Python related permission issue, try deleting the directory manually.")
        elif e.errno != errno.ENOENT: # errno.ENOENT = no such file or directory
            raise # re-raise exception if a different error occurred

# python input has renamed in 3.x without deprecation-fallback
# since there is still a lot 2.x out there, we need to try and error
# also, let's just make it return a boolean a.k.a confirmation
def get_confirmation(text):
    try :
        return True if raw_input(text) is 'y' else False
    except:
        return True if input(text) is 'y' else False

# and one more to help argparse being useful
def str2bool(v):
    if isinstance(v, bool):
        return v
    if v.lower() in ('yes', 'true', 't', 'y', '1'):
        return True
    elif v.lower() in ('no', 'false', 'f', 'n', '0'):
        return False
    else:
        raise argparse.ArgumentTypeError('Boolean value expected.')

################################################################################################# ARGUMENTS

# jesus, why is the syntax of such a pain in the ARgparSE
# anyway, parse the arguments
parser = argparse.ArgumentParser(description='Okay, so if you want to have an argument...')
parser.add_argument('-d', '--delete', type=str2bool, nargs='?', const=True, default=False, help='delete old project files etc without asking.')
parser.add_argument('-c', '--create', type=str2bool, nargs='?', const=True, default=False, help='create new project files etc without asking. Watch out with Windows.')
parser.add_argument('-a', '--addon', type=str, default='ofxBulletRaw', help='the name of this ofxAddon.')
args = vars(parser.parse_args())

# set globals
ADDON_NAME = args['addon']

# ask questions
print('shall we remove all qtcreator build directories build-*?')
removeQtBuildDirectories = True if args['delete'] else get_confirmation('(y/n) --> ')
print('shall we remove all qt creator project files?')
removeQtProjectFiles = True if args['delete'] else get_confirmation('(y/n) --> ')
print('shall we remove all visual studio project files?')
removeVsProjectFiles = True if args['delete'] else get_confirmation('(y/n) --> ')
print('shall we create project files with projectGenerator? Watch out with Windows.')
createWithPG = True if args['create'] else get_confirmation('(y/n) --> ')

################################################################################################# MAIN
# this should run without errors or user interaction

if removeQtBuildDirectories:
    for exampleDir in glob.glob(r'./build-*'):
        print('removing {}'.format(exampleDir))
        silentremove(exampleDir)

for exampleDir in glob.glob(r'./*xample*'):
    print('cleaning {}'.format(exampleDir))
    silentremove(exampleDir + '/Makefile')
    silentremove(exampleDir + '/config.make')
    silentremove(exampleDir + '/obj')

    if (removeQtProjectFiles):
        for filepath in glob.glob(exampleDir + '/*.qbs*'):
            silentremove(filepath)

    if (removeVsProjectFiles):
        for filepath in glob.glob(exampleDir + '/*.sln'):
            silentremove(filepath)
        for filepath in glob.glob(exampleDir + '/*.vcxproj*'):
            silentremove(filepath)

    if (createWithPG):
        addonsMake = exampleDir + '/addons.make'
        if os.path.isfile(addonsMake):
            with open(addonsMake) as f:
                addons = f.readlines()
                addons = [x.strip() for x in addons]

                cmd = 'projectGenerator -a"{}" {}'.format(', '.join(addons), exampleDir)
                print(cmd)
                os.system(cmd)
        else:
            cmd = 'projectGenerator -a"{}" {}'.format(ADDON_NAME, exampleDir)
            os.system(cmd)

print("""
                       ______
                      /\     \ 
                     /  \  I  \ 
                    /    \_____\ 
                   _\    / ____/_
                  /\ \  / /\     \ 
                 /  \ \/_/  \  T  \ 
                /    \__/    \_____\ 
               _\    /  \    / ____/_
              /\ \  /    \  / /\     \ 
             /  \ \/_____/\/_/  \     \ 
            /    \_____\    /    \_____\ 
           _\    /     /    \    / ____/_
          /\ \  /  I  /      \  / /\     \ 
         /  \ \/_____/        \/_/  \     \ 
        /    \_____\            /    \_____\ 
       _\    /     /            \    / ____/_
      /\ \  /  S  /              \  / /\     \ 
     /  \ \/_____/                \/_/  \     \ 
    /    \_____\                    /    \_____\ 
   _\    /     /_  ______  ______  _\____/ ____/_
  /\ \  /     /  \/\     \/\     \/\     \/\     \ 
 /  \ \/_____/    \ \     \ \     \ \     \ \     \ 
/    \_____\ \_____\ \_____\ \_____\ \_____\ \_____\ 
\    /     / /     / /     / /     / /     / /     /
 \  /     / /  D  / /  O  / /  N  / /  E  / /     /
  \/_____/\/_____/\/_____/\/_____/\/_____/\/_____/

  """)

